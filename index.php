<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>后台管理</title>
<link rel="stylesheet" type="text/css"  href="extjs/resources/css/ext-all.css" />
<script type="text/javascript"  src="extjs/ext-all-debug.js"></script>
<script type="text/javascript"   src="js/index.js"></script>
    

<style type="text/css">
p {
    margin:5px;
}
.settings {
    background-image:url(icons/folder_wrench.png);
}
.nav {
    background-image:url(icons/folder_go.png);
}
.info {
    background-image:url(icons/information.png);
}
</style>

</head>
<body>
    <!-- use class="x-hide-display" to prevent a brief flicker of the content -->
    <div id="west" class="x-hide-display">
       
    </div>
    <div id="home" class="x-hide-display">
        
        	<div style="text-align: center; padding-top: 12%">
				<font color="#F61D62" size="10"> 欢迎使用党员信息管理平台 </font>
			</div>
    </div>
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;">
    </div>
    <div id="south" class="x-hide-display">
        <p>south - generally for informational stuff, also could be for status bar</p>
    </div>
</body>
</html>
