// JavaScript Document
Ext.onReady(function(){
	
	Ext.QuickTips.init();
    var proxy = new Ext.data.HttpProxy({
        url: 'data.php',
    	method: 'post'
    });
	
	var pbar = new Ext.PagingToolbar
	({
		  emptyMsg:"没有数据",
		  displayInfo:true,
		  displayMsg:"显示从{0}条数据到{1}条数据，共{2}条数据",
		  store:dstore,
		  pageSize:10
	 });
	
    var dstore = new Ext.data.JsonStore({
    url: 'data.php',
    proxy: proxy,
    fields: ['id', 'data'],
    totalProperty: 'totalCount',
    root: 'matches'
    });
    dstore.load();

    var grid = new Ext.grid.GridPanel({
    	store: dstore,
        stripeRows: true,
        autoExpandColumn: 'data',
        width: 800,
        title:'详细数据',
		align : 'center',
		bbar:new Ext.PagingToolbar({     
            pageSize: 10,     
            store: dstore,     
            displayInfo: true,     
            beforePageText : '第',      
            afterPageText : '页&nbsp;共 {0} 页',      
            refreshText : '刷新',     
            displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',     
            emptyMsg: "没有记录"              
        }),
		dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                ui: 'footer',
                items: ['->', {
                    iconCls: 'icon-save',
                    itemId: 'save',
                    text: 'Save',
                    disabled: true,
                    scope: this,
                    handler: this.onSave
                }, {
                    iconCls: 'icon-user-add',
                    text: 'Create',
                    scope: this,
                    handler: this.onCreate
                }, {
                    iconCls: 'icon-reset',
                    text: 'Reset',
                    scope: this,
                    handler: this.onReset
                }]
            }],
		columns: [
        {id:'id', header: "Id", width:200,  sortable: true, dataIndex: 'id',text:'id'},
        {header: "数据", width: 500, sortable: true, dataIndex: 'data',text:'data'}
        ],
    });
	grid.render('data');
	
	});


