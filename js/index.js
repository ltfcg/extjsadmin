Ext.require(['*']);
	
    Ext.onReady(function() {
        Ext.QuickTips.init();
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));	
		var store = Ext.create('Ext.data.TreeStore', {
    	root: {
        expanded: true,
        children: [
				{ text: "登录", leaf: true,url:"login.php"},
				{ text: "数据表格", leaf: true,url:"datagrid.php"},
				{ text: "信息类", leaf: true,url:"http://www.oschina.com" }
			]
			}
		});
		var tree = Ext.create('Ext.tree.Panel', {
							width: 200,
							fit:true,
							store: store,
							rootVisible: false
/*							listeners:{
								 itemclick: function(view, record, item, index, e)
								 {
									 
									Ext.getCmp('maintab').add({title:record.data.text,html:"helloword",closable:true});
								 }
			
								}*/
							});
		var maintab =    Ext.create('Ext.tab.Panel', {
				id:'maintab',
                region: 'center',  
                deferredRender: false,
                activeTab: 0,     
                items: [{
                    contentEl: 'home',
                    title: 'home',
                    autoScroll: true
                }]
            });					
        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
			//north 为头部
            Ext.create('Ext.Component', {
                region: 'north',
                height: 47, 
                autoEl: {
                    tag: 'div',
                    html:'<img id="img_left" src="images/main_06.gif"/> <img id="img_left" src="images/main_07.gif"/> <img id="img_left" src="images/main_08.gif"/> '
                }
            }), 
			
			//左侧的菜单，
			{
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel', // see Ext.getCmp() below
                title: '操作菜单',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'accordion',
                items: [{
                    contentEl: 'west',
                    title: '导航',
                    iconCls: 'nav',
					items:[tree] 
                }, {
                    title: '设置',
                    html: '<p>Some settings in here.</p>',
                    iconCls: 'settings'
                }, {
                    title: '信息',
                    html: '<p>Some info in here.</p>',
                    iconCls: 'info'
                }]
            },
			//中间的内容
			maintab
         ]
        });
		
		function addTab(text,path) {
				var tabName = "tab_" + text;
				var tab = maintab.getComponent(tabName);
				if (!tab) {
					tab = maintab.add({
						id : tabName,
						name : tabName,
						title : text,
						closable : true,
						//loader : {
						html : "<iframe frameborder=0 scrolling='auto' style='width:100%;height:100%' src='"+path+"'</iframe>",
						//	loadMask : "loading...",
						//	autoLoad : true,
						//	scripts : true
						//}
					}).show();
				} else {
					maintab.setActiveTab(tab);
				}
    	}
	
		tree.on('itemclick',function(view,record,item,index,e)
		{
			addTab(record.data.text,record.raw.url);
		});
		
    });